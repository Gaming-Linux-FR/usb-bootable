# Création d'un support USB bootable

Pour créer une clé USB bootable, qui vous permettra d'installer n'importe quel système d'exploitation, vous aurez besoin d'une clé USB d'au moins 8 Go et de télécharger l'outil [BalenaEtcher](https://etcher.balena.io/#download-etcher), disponible sur Windows, Mac, et Linux. Cet outil simplifie le processus de création d'un support d'installation bootable.

## Étape 1 : Préparer l'image avec Balena Etcher

1. **Lancez Balena Etcher.** Vous accéderez à l'interface principale du logiciel, qui devrait ressembler à ceci :

    ![BalenaEtcher_1](Images/BalenaEtcher_1.png)

    Cette interface vous permettra de configurer votre clé USB.

2. **Chargez votre fichier image.** Cliquez sur le bouton "Flash from file" pour naviguer vers et sélectionner le fichier image disque (généralement un fichier ISO) que vous avez préalablement téléchargé pour le système d'exploitation que vous souhaitez installer.

    Après avoir sélectionné le fichier, l'interface affichera que l'image est prête à être utilisée :

    ![BalenaEtcher_2](Images/BalenaEtcher_2.png)

3. **Sélectionnez votre clé USB comme cible.** Cliquez sur "Select target" pour choisir la clé USB sur laquelle vous voulez installer l'image. Assurez-vous que votre clé USB est correctement connectée à votre ordinateur.

    Vous verrez alors les périphériques de stockage disponibles. Sélectionnez votre clé USB :

    ![BalenaEtcher_3](Images/BalenaEtcher_3.png)

    Confirmez votre choix en cliquant sur "Select 1" :

    ![BalenaEtcher_4](Images/BalenaEtcher_4.png)

4. **Commencez le processus de création.** Cliquez sur "Flash!" pour démarrer la copie de l'image disque sur la clé USB. La durée de cette opération dépendra de la vitesse de votre clé USB.

    Une barre de progression indiquera l'avancement du processus :

    ![BalenaEtcher_5](Images/BalenaEtcher_5.png)

5. **Vérification et finalisation.**

Une fois la copie terminée, Balena Etcher effectuera automatiquement une vérification pour s'assurer que tout s'est bien passé, confirmant ainsi que la clé USB est prête à l'emploi.

Vous verrez un message de confirmation une fois le processus terminé, indiquant que votre clé USB bootable est prête. Vous pouvez maintenant l'utiliser pour démarrer et installer le système d'exploitation choisi sur un ordinateur.

## Étape 2 : Démarrer sur la clé USB

Maintenant que votre clé USB est prête, il est temps de démarrer votre ordinateur à partir de celle-ci pour installer Ubuntu. Suivez ces étapes simples :

1. **Insérez la clé USB** : Branchez votre clé USB bootable dans un port USB de votre ordinateur.

2. **Redémarrez votre ordinateur** : Éteignez votre ordinateur puis rallumez-le. Soyez attentif au démarrage pour accéder au menu de sélection du périphérique de démarrage.

3. **Accédez au menu de démarrage** : Immédiatement après l'allumage de l'ordinateur, appuyez sur la touche appropriée pour ouvrir le menu de démarrage. Cette touche varie selon le fabricant de l'ordinateur et est souvent indiquée brièvement à l'écran lors du démarrage, comme `F12`, `F10`, `F9`, `F8`, ou `Esc`. Si vous ne voyez pas cette information ou si vous manquez le moment, redémarrez l'ordinateur et essayez à nouveau.

4. **Sélectionnez la clé USB dans le menu de démarrage** : Utilisez les flèches de votre clavier pour naviguer dans le menu et sélectionner votre clé USB. Le nom peut varier, mais il comprend généralement le mot "USB" ou le nom du fabricant de la clé.

5. **Confirmez le démarrage sur la clé USB** : Suivez les instructions à l'écran pour démarrer à partir de la clé USB. Si tout est correct, votre ordinateur devrait charger l'installateur Ubuntu à partir de la clé.

### Conseils supplémentaires :

- **Vérifiez les paramètres du BIOS/UEFI** : Si votre ordinateur ne propose pas directement un menu de démarrage rapide, vous devrez peut-être entrer dans le BIOS ou l'UEFI pour modifier l'ordre de démarrage. Cela vous permettra de placer la clé USB en première position avant les autres disques durs ou périphériques de démarrage.
- **Mode de démarrage sécurisé** : Beaucoup de distributions Linux ne sont pas compatibles [secureboot](https://learn.microsoft.com/fr-fr/windows-hardware/design/device-experiences/oem-secure-boot) Si vous avez des problèmes pour démarrer à partir de la clé USB, assurez-vous que le mode de démarrage sécurisé (Secure Boot) est désactivé dans le BIOS/UEFI, car cela peut empêcher le démarrage à partir de périphériques non reconnus.